package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.MapFunction;

import de.fraunhofer.igd.geo.vgv.flink.Point;

public class PointParser implements MapFunction<String, Point> {

	private static final long serialVersionUID = -3389358031503714243L;

	@Override
	public Point map(String value) throws Exception {
		return Point.fromString(value);
	}

}
