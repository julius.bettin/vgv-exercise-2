package de.fraunhofer.igd.geo.vgv.flink;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;

import de.fraunhofer.igd.geo.vgv.flink.transformation.BoundingBoxInitializer;
import de.fraunhofer.igd.geo.vgv.flink.transformation.BoundingBoxReducer;
import de.fraunhofer.igd.geo.vgv.flink.transformation.GridCellAssigner;
import de.fraunhofer.igd.geo.vgv.flink.transformation.PointFilter;
import de.fraunhofer.igd.geo.vgv.flink.transformation.PointFormatter;
import de.fraunhofer.igd.geo.vgv.flink.transformation.PointParser;
import de.fraunhofer.igd.geo.vgv.flink.transformation.PrefixMarker;
import de.fraunhofer.igd.geo.vgv.flink.transformation.TupleExtractor;

public class PointCloudLOD {
	
	private static final String convertDurationToHumanReadableFormat(Duration duration) {
		// working that Java 8(TM) magic...
		long s = duration.getSeconds();
	    return String.format("%02dh %02dm %02ds", s / 3600, (s % 3600) / 60, (s % 60));
	}
	
    public static void main(String... args) throws Exception {
        ParameterTool params = ParameterTool.fromArgs(args);
        String dataFile = ""; // path to the point cloud file
        String outDir = "";
        if (params.has("data")) {
            dataFile = params.get("data");
        } else {
            throw new RuntimeException("No --data attribute passed. Set the location of the input data file.");
        }
        
        if (params.has("out")) {
        	outDir = params.get("out");
        } else {
        	throw new RuntimeException("No --out attribute passed. Set the location of the output files.");
        }
        
        // time measurement:
        Instant start = Instant.now();
        
        System.out.println("Calculating bounding box...");

        // 0. set up execution environment
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // 1. read and parse source data
        DataSource<String> dataSource = env.readTextFile(dataFile);
        DataSet<Point> pointSet = dataSource.map(new PointParser());
        
        // 2. calculate bounding box;
        //    the first element in the tuple represents the lower bound,
        //    the seconds element represents the upper bound
        Tuple2<Vector3, Vector3> boundingBoxResult = pointSet
        		// 2.a initialize bounding box data:
        		//     each point is mapped to a tuple where lower bound == upper bound == point
        		.map(new BoundingBoxInitializer())
        		// 2.b calculate the global lower bound and upper bound
        		.reduce(new BoundingBoxReducer())
        		.collect()
        		.get(0);
        
        System.out.println(String.format("Done: (x = %f, y = %f, z = %f, width = %f, height = %f, depth = %f)",
        		boundingBoxResult.f0.x, boundingBoxResult.f0.y, boundingBoxResult.f0.z,
        		boundingBoxResult.f1.x - boundingBoxResult.f0.x, boundingBoxResult.f1.y - boundingBoxResult.f0.y, boundingBoxResult.f1.z - boundingBoxResult.f0.z));
        
        final Rectangle boundingBox = new Rectangle(boundingBoxResult);
        
        
        int cellCount = 10;
        int currentLayer = 1;
        long numPointsLeft = -1;
        
        // 3. iteratively process layers of points until there are no more points
        while (numPointsLeft > 0 || currentLayer == 1) {
        	System.out.println(String.format("Processing Layer %d (%d x %d)...", currentLayer, cellCount, cellCount));
        	
        	final Grid grid = new Grid(boundingBox, cellCount, cellCount);
        	
        	// 3.a assign points to cells in the grid and mark the first 100 per cell
        	DataSet<Tuple2<Boolean, Point>> markedGridSet = pointSet
        		.map(new GridCellAssigner(boundingBox, grid))
        		.groupBy(0, 1)
        		.reduceGroup(new PrefixMarker(100));
        	
        	// 3.b write the marked points to a file
        	Path filePath = Paths.get(outDir, "layer_" + currentLayer + ".xyz");
        	markedGridSet.filter(new PointFilter(true))
        		.writeAsFormattedText(filePath.toString(), new PointFormatter())
        		.setParallelism(1);
        	
        	// 3.c select the unmarked points for further processing...
        	pointSet = markedGridSet.filter(new PointFilter(false))
        		// ...and strip them off the Boolean marker
        		.map(new TupleExtractor());
        	
        	// 3.d increase layer number and cell count
        	currentLayer++;
        	cellCount *= 2;
        	numPointsLeft = pointSet.count();
        	System.out.println(String.format("==> %d points for further processing", numPointsLeft));
        }
        
        Instant finish = Instant.now();
        Duration duration = Duration.between(start, finish);
        
        System.out.println(String.format("Done; Time elapsed: %s", convertDurationToHumanReadableFormat(duration)));
        
    }
}
