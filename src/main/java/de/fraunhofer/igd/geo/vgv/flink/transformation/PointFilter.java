package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import de.fraunhofer.igd.geo.vgv.flink.Point;

public class PointFilter implements FilterFunction<Tuple2<Boolean,Point>> {

	private static final long serialVersionUID = -34323533455401443L;
	
	private boolean inLayer;
	
	public PointFilter(boolean inLayer) {
		this.inLayer = inLayer;
	}

	@Override
	public boolean filter(Tuple2<Boolean, Point> value) throws Exception {
		return value.f0 == inLayer;
	}

}
