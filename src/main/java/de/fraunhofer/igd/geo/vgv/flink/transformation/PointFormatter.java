package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.java.io.TextOutputFormat.TextFormatter;
import org.apache.flink.api.java.tuple.Tuple2;

import de.fraunhofer.igd.geo.vgv.flink.Point;

public class PointFormatter implements TextFormatter<Tuple2<Boolean,Point>> {

	private static final long serialVersionUID = 6589971486151207827L;

	@Override
	public String format(Tuple2<Boolean, Point> value) {
		return value.f1.toString();
	}

}
