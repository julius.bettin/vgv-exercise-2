package de.fraunhofer.igd.geo.vgv.flink;

public class Grid {
	
	public final int cellCountWidth;
	public final int cellCountHeight;
	
	public final double cellWidth;
	public final double cellHeight;
	
	public Grid(final Rectangle boundingBox, int cellCountWidth, int cellCountHeight) {
		this.cellCountWidth = cellCountWidth;
		this.cellCountHeight = cellCountHeight;
		cellWidth = boundingBox.width / cellCountWidth;
		cellHeight = boundingBox.height / cellCountHeight;
	}

}
