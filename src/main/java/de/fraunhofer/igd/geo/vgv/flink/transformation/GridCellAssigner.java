package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;

import de.fraunhofer.igd.geo.vgv.flink.Grid;
import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.Rectangle;

public class GridCellAssigner implements MapFunction<Point, Tuple3<Integer, Integer, Point>> {

	private static final long serialVersionUID = -4319215056480170604L;
	
	private final double originX;
	private final double originY;
	private final double cellWidth;
	private final double cellHeight;
	private final int cellCountWidth;
	private final int cellCountHeight;
	
	public GridCellAssigner(final Rectangle boundingBox, final Grid grid) {
		originX = boundingBox.x;
		originY = boundingBox.y;
		cellWidth = grid.cellWidth;
		cellHeight = grid.cellHeight;
		cellCountWidth = grid.cellCountWidth;
		cellCountHeight = grid.cellCountHeight;
	}

	@Override
	public Tuple3<Integer, Integer, Point> map(Point value) throws Exception {
		double offsetX = value.x - originX;
		double offsetY = value.y - originY;
		int gridX = Math.max(0, Math.min((int) (offsetX / cellWidth), cellCountWidth - 1));
		int gridY = Math.max(0, Math.min((int) (offsetY / cellHeight), cellCountHeight - 1));
		return new Tuple3<>(gridX, gridY, value);
	}

}
