package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import de.fraunhofer.igd.geo.vgv.flink.Point;

public class TupleExtractor implements MapFunction<Tuple2<Boolean,Point>, Point> {

	private static final long serialVersionUID = 5454772446005000355L;

	@Override
	public Point map(Tuple2<Boolean, Point> value) throws Exception {
		return value.f1;
	}

}
