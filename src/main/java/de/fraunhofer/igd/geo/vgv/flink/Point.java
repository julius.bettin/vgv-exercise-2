package de.fraunhofer.igd.geo.vgv.flink;

public class Point { // extends Vector3 {
	
	public final double x;
	public final double y;
	public final double z;
	public final double pointSourceId;
	public final double userData;
	public final double scanAngleRank;
	public final double flightLineEdge;
	public final double numberOfReturns;
	public final double returnNumber;
	public final double time;
	public final double intensity;
	public final double classification;
	
	public static Point fromString(String str) {
		String parts[] = str.split(" ");
		if (parts.length != 12) {
			throw new IllegalArgumentException();
		}
		return new Point(Double.parseDouble(parts[0]), Double.parseDouble(parts[1]), Double.parseDouble(parts[2]),
				Double.parseDouble(parts[3]), Double.parseDouble(parts[4]), Double.parseDouble(parts[5]),
				Double.parseDouble(parts[6]), Double.parseDouble(parts[7]), Double.parseDouble(parts[8]),
				Double.parseDouble(parts[9]), Double.parseDouble(parts[10]), Double.parseDouble(parts[11]));
	}

	public Point(double x, double y, double z, double pointSourceId, double userData, double scanAngleRank,
			double flightLineEdge, double numberOfReturns, double returnNumber, double time, double intensity,
			double classification) {
//		super(x, y, z);
		this.x = x;
		this.y = y;
		this.z = z;
		this.pointSourceId = pointSourceId;
		this.userData = userData;
		this.scanAngleRank = scanAngleRank;
		this.flightLineEdge = flightLineEdge;
		this.numberOfReturns = numberOfReturns;
		this.returnNumber = returnNumber;
		this.time = time;
		this.intensity = intensity;
		this.classification = classification;
	}
	
	public Vector3 toVector() {
		return new Vector3(x, y, z);
	}
	
	@Override
	public String toString() {
		return String.format("%f %f %f %f %f %f %f %f %f %f %f %f", x, y, z, pointSourceId, userData, scanAngleRank, flightLineEdge, numberOfReturns, returnNumber, time, intensity, classification);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Point)) {
			return false;
		}
		Point other = (Point) obj;
		// STRICT equality, no epsilon involved!
		return x == other.x &&
				y == other.y &&
				z == other.z &&
				pointSourceId == other.pointSourceId &&
				userData == other.userData &&
				scanAngleRank == other.scanAngleRank &&
				flightLineEdge == other.flightLineEdge &&
				numberOfReturns == other.numberOfReturns &&
				returnNumber == other.returnNumber &&
				time == other.time &&
				intensity == other.intensity &&
				classification == other.classification;
	}

//	public double getX() {
//		return x;
//	}
//	public double getY() {
//		return y;
//	}
//	public double getZ() {
//		return z;
//	}
//	public double getPointSourceId() {
//		return pointSourceId;
//	}
//	public double getUserData() {
//		return userData;
//	}
//	public double getScanAngleRank() {
//		return scanAngleRank;
//	}
//	public double getFlightLineEdge() {
//		return flightLineEdge;
//	}
//	public double getNumberOfReturns() {
//		return numberOfReturns;
//	}
//	public double getReturnNumber() {
//		return returnNumber;
//	}
//	public double getTime() {
//		return time;
//	}
//	public double getIntensity() {
//		return intensity;
//	}
//	public double getClassification() {
//		return classification;
//	}
}
