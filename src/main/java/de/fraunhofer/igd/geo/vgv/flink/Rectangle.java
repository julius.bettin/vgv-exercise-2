package de.fraunhofer.igd.geo.vgv.flink;

import org.apache.flink.api.java.tuple.Tuple2;

public class Rectangle {
	
	public final double x;
	public final double y;
	public final double width;
	public final double height;
	
	public Rectangle(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public Rectangle(Tuple2<Vector3, Vector3> boundaries) {
		x = boundaries.f0.x;
		y = boundaries.f0.y;
		width = boundaries.f1.x - x;
		height = boundaries.f1.y - y;
	}

}
