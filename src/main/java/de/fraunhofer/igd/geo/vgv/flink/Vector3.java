package de.fraunhofer.igd.geo.vgv.flink;

public class Vector3 {
	
	public final double x;
	public final double y;
	public final double z;
	
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3 min(Vector3 other) {
		double minX = Math.min(x, other.x);
		double minY = Math.min(y, other.y);
		double minZ = Math.min(z, other.z);
		return new Vector3(minX, minY, minZ);
	}
	
	public Vector3 max(Vector3 other) {
		double maxX = Math.max(x, other.x);
		double maxY = Math.max(y, other.y);
		double maxZ = Math.max(z, other.z);
		return new Vector3(maxX, maxY, maxZ);
	}
	
	@Override
	public String toString() {
		return String.format("Vector3(%f, %f, %f)", x, y, z);
	}

}
