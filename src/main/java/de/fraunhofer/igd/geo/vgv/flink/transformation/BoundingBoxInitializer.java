package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.Vector3;

public class BoundingBoxInitializer implements MapFunction<Point, Tuple2<Vector3, Vector3>> {

	private static final long serialVersionUID = 4996500703693228806L;

	@Override
	public Tuple2<Vector3, Vector3> map(Point value) throws Exception {
		return new Tuple2<>(value.toVector(), value.toVector());
	}

}
