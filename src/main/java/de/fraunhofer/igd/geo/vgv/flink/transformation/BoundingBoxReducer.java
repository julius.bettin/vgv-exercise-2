package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import de.fraunhofer.igd.geo.vgv.flink.Vector3;

public class BoundingBoxReducer implements ReduceFunction<Tuple2<Vector3,Vector3>> {

	private static final long serialVersionUID = 1144377692852251175L;

	@Override
	public Tuple2<Vector3, Vector3> reduce(Tuple2<Vector3, Vector3> value1, Tuple2<Vector3, Vector3> value2)
			throws Exception {
		return new Tuple2<>(value1.f0.min(value2.f0), value1.f1.max(value2.f1));
	}

}
