package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;

import de.fraunhofer.igd.geo.vgv.flink.Point;

public class PrefixMarker implements GroupReduceFunction<Tuple3<Integer,Integer,Point>, Tuple2<Boolean, Point>> {

	private static final long serialVersionUID = 513995707098605054L;
	
	private int prefixLength;
	
	public PrefixMarker(int prefixLength) {
		this.prefixLength = prefixLength;
	}

	@Override
	public void reduce(Iterable<Tuple3<Integer, Integer, Point>> values, Collector<Tuple2<Boolean, Point>> out)
			throws Exception {
		int spaceLeftInCurrentLayer = prefixLength;
		for (Tuple3<Integer, Integer, Point> value : values) {
			if (spaceLeftInCurrentLayer-- > 0) {
				out.collect(new Tuple2<>(true, value.f2));
			} else {
				out.collect(new Tuple2<>(false, value.f2));
			}
		}
	}

}
