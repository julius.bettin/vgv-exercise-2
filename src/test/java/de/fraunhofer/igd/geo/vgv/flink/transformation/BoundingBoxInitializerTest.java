package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.PointTest;
import de.fraunhofer.igd.geo.vgv.flink.Vector3;

public class BoundingBoxInitializerTest {

	@Test
	public void testInitializesBoundBoxCorrectly() throws Exception {
		BoundingBoxInitializer initializer = new BoundingBoxInitializer();
		Point point = Point.fromString(PointTest.POINT_STRING);
		Tuple2<Vector3, Vector3> result = initializer.map(point);
		assertEquals(point.x, result.f0.x);
		assertEquals(point.y, result.f0.y);
		assertEquals(point.z, result.f0.z);
		assertEquals(point.x, result.f1.x);
		assertEquals(point.y, result.f1.y);
		assertEquals(point.z, result.f1.z);
	}
	
}
