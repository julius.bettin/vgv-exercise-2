package de.fraunhofer.igd.geo.vgv.flink;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.util.PointFactory;

public class PointTest {

	public static final String POINT_STRING = "123726.250000 480388.390000 9.150000 30812.000000 2.000000 -22.000000 0.000000 4.000000 1.000000 329225.017425 21.000000 1.000000";

	public static final double POINT_X = 123726.250000;
	public static final double POINT_Y = 480388.390000;
	public static final double POINT_Z = 9.150000;
	public static final double POINT_SOURCE_ID = 30812.000000;
	public static final double POINT_USER_DATA = 2.000000;
	public static final double POINT_SCAN_ANGLE_RANK = -22.000000;
	public static final double POINT_FLIGHT_LINE_EDGE = 0.000000;
	public static final double POINT_NUMBER_OF_RETURNS = 4.000000;
	public static final double POINT_RETURN_NUMBER = 1.000000;
	public static final double POINT_TIME = 329225.017425;
	public static final double POINT_INTENSITY = 21.000000;
	public static final double POINT_CLASSIFICATION = 1.000000;

	@Test
	public void testInitializesCorrectlyFromString() {
		Point point = Point.fromString(POINT_STRING);
		assertEquals(POINT_X, point.x);
		assertEquals(POINT_Y, point.y);
		assertEquals(POINT_Z, point.z);
		assertEquals(POINT_SOURCE_ID, point.pointSourceId);
		assertEquals(POINT_USER_DATA, point.userData);
		assertEquals(POINT_SCAN_ANGLE_RANK, point.scanAngleRank);
		assertEquals(POINT_FLIGHT_LINE_EDGE, point.flightLineEdge);
		assertEquals(POINT_NUMBER_OF_RETURNS, point.numberOfReturns);
		assertEquals(POINT_RETURN_NUMBER, point.returnNumber);
		assertEquals(POINT_TIME, point.time);
		assertEquals(POINT_INTENSITY, point.intensity);
		assertEquals(POINT_CLASSIFICATION, point.classification);
	}

	@Test
	public void testSerializesCorrectlyToString() {
		Point point = new Point(POINT_X, POINT_Y, POINT_Z, POINT_SOURCE_ID, POINT_USER_DATA, POINT_SCAN_ANGLE_RANK,
				POINT_FLIGHT_LINE_EDGE, POINT_NUMBER_OF_RETURNS, POINT_RETURN_NUMBER, POINT_TIME, POINT_INTENSITY,
				POINT_CLASSIFICATION);
		assertEquals(POINT_STRING, point.toString());
	}
	
	@Test
	public void testConvertsCorrectlyToVector3() {
		Point point = Point.fromString(POINT_STRING);
		Vector3 vector = point.toVector();
		assertEquals(POINT_X, vector.x);
		assertEquals(POINT_Y, vector.y);
		assertEquals(POINT_Z, vector.z);
	}
	
	@Test
	public void testPointEquality() {
		Point point = Point.fromString(POINT_STRING);
		assertTrue(point.equals(point));
	}
	
	@Test
	public void testPointInequality() {
		Point point1 = Point.fromString(POINT_STRING);
		Point point2 = PointFactory.createPoint(10.0, 20.0, 30.0);
		assertFalse(point1.equals(point2));
		assertFalse(point2.equals(point1));
	}

}
