package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.PointTest;

public class PointParserTest {

	@Test
	public void testParsesPointCorrectly() throws Exception {
		// TODO: this test is a plain copy from the PointTest class...
		PointParser parser = new PointParser();
		Point point = parser.map(PointTest.POINT_STRING);
		assertEquals(PointTest.POINT_X, point.x);
		assertEquals(PointTest.POINT_Y, point.y);
		assertEquals(PointTest.POINT_Z, point.z);
		assertEquals(PointTest.POINT_SOURCE_ID, point.pointSourceId);
		assertEquals(PointTest.POINT_USER_DATA, point.userData);
		assertEquals(PointTest.POINT_SCAN_ANGLE_RANK, point.scanAngleRank);
		assertEquals(PointTest.POINT_FLIGHT_LINE_EDGE, point.flightLineEdge);
		assertEquals(PointTest.POINT_NUMBER_OF_RETURNS, point.numberOfReturns);
		assertEquals(PointTest.POINT_RETURN_NUMBER, point.returnNumber);
		assertEquals(PointTest.POINT_TIME, point.time);
		assertEquals(PointTest.POINT_INTENSITY, point.intensity);
		assertEquals(PointTest.POINT_CLASSIFICATION, point.classification);
	}

}
