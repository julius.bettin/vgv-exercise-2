package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.util.MockCollector;
import de.fraunhofer.igd.geo.vgv.flink.util.PointFactory;

public class PrefixMarkerTest {

	@Test
	public void testMarksCorrectAmount() throws Exception {
		List<Tuple3<Integer, Integer, Point>> points = new ArrayList<>(5);
		points.add(new Tuple3<Integer, Integer, Point>(0, 0, PointFactory.createPoint(0.0, 0.0, 0.0)));
		points.add(new Tuple3<Integer, Integer, Point>(0, 0, PointFactory.createPoint(1.0, 1.0, 1.0)));
		points.add(new Tuple3<Integer, Integer, Point>(0, 0, PointFactory.createPoint(2.0, 2.0, 2.0)));
		points.add(new Tuple3<Integer, Integer, Point>(0, 0, PointFactory.createPoint(3.0, 3.0, 3.0)));
		points.add(new Tuple3<Integer, Integer, Point>(0, 0, PointFactory.createPoint(4.0, 4.0, 4.0)));
		
		PrefixMarker marker = new PrefixMarker(3);
		MockCollector<Tuple2<Boolean, Point>> collector = new MockCollector<>();
		marker.reduce(points, collector);
		
		assertEquals(5, collector.getRecordCount());
		assertEquals(true, collector.getRecord(0).f0);
		assertEquals(true, collector.getRecord(1).f0);
		assertEquals(true, collector.getRecord(2).f0);
		assertEquals(false, collector.getRecord(3).f0);
		assertEquals(false, collector.getRecord(4).f0);
	}
	
}
