package de.fraunhofer.igd.geo.vgv.flink.util;

import de.fraunhofer.igd.geo.vgv.flink.Point;

public class PointFactory {
	
	public static Point createPoint(double x, double y, double z) {
		return new Point(x, y, z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}

}
