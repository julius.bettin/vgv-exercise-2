package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.flink.api.java.tuple.Tuple3;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Grid;
import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.Rectangle;
import de.fraunhofer.igd.geo.vgv.flink.util.PointFactory;

public class GridCellAssignerTest {
	
	private final Rectangle boundingBox = new Rectangle(10.0, 20.0, 100.0, 50.0);
	private final Grid grid = new Grid(boundingBox, 10, 10);
	private final GridCellAssigner assigner = new GridCellAssigner(boundingBox, grid);
	
	@Test
	public void testAssignsPointInsideBoundingBoxCorrectly() throws Exception {
		Point point = PointFactory.createPoint(11.0, 21.0, 0.0);
		Tuple3<Integer, Integer, Point> result = assigner.map(point);
		assertEquals(0, result.f0.intValue());
		assertEquals(0, result.f1.intValue());
		assertEquals(point, result.f2);
	}

}
