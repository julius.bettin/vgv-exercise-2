package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Vector3;

public class BoundingBoxReducerTest {

	@Test
	public void testsSelectsMinAndMaxCorrectly() throws Exception {
		Vector3 lowerBound1 = new Vector3(10.0, 10.0, 10.0);
		Vector3 upperBound1 = new Vector3(20.0, 20.0, 20.0);
		Tuple2<Vector3, Vector3> boundingBox1 = new Tuple2<>(lowerBound1, upperBound1);
		Vector3 lowerBound2 = new Vector3(5.0, 10.0, 20.0);
		Vector3 upperBound2 = new Vector3(15.0, 20.0, 25.0);
		Tuple2<Vector3, Vector3> boundingBox2 = new Tuple2<>(lowerBound2, upperBound2);
		
		BoundingBoxReducer reducer = new BoundingBoxReducer();
		Tuple2<Vector3, Vector3> result = reducer.reduce(boundingBox1, boundingBox2);
		// lower bound
		assertEquals(5.0, result.f0.x);
		assertEquals(10.0, result.f0.y);
		assertEquals(10.0, result.f0.z);
		// upper bound
		assertEquals(20.0, result.f1.x);
		assertEquals(20.0, result.f1.y);
		assertEquals(25.0, result.f1.z);
	}
	
}
