package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.PointTest;

public class PointFormatterTest {

	@Test
	public void testFormatsPointCorrectly() {
		Point point = Point.fromString(PointTest.POINT_STRING);
		Tuple2<Boolean, Point> value = new Tuple2<>(true, point);
		PointFormatter formatter = new PointFormatter();
		String result = formatter.format(value);
		assertEquals(PointTest.POINT_STRING, result);
		// TODO: Comparing the strings directly is not the best choice,
		// as the amount of trailing zeros might change
	}
	
}
