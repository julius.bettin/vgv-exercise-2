package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.util.PointFactory;

public class PointFilterTest {
	
	private static final Tuple2<Boolean, Point> TRUTHY_POINT = new Tuple2<>(true, PointFactory.createPoint(0.0, 0.0, 0.0));
	private static final Tuple2<Boolean, Point> FALSY_POINT = new Tuple2<>(false, PointFactory.createPoint(0.0, 0.0, 0.0));
	
	@Test
	public void testAcceptsTruthyPoint() throws Exception {
		PointFilter filter = new PointFilter(true);
		assertTrue(filter.filter(TRUTHY_POINT));
	}
	
	@Test
	public void testAcceptsFalsyPoint() throws Exception {
		PointFilter filter = new PointFilter(false);
		assertTrue(filter.filter(FALSY_POINT));
	}
	
	@Test
	public void testRejectsTruthyPoint() throws Exception {
		PointFilter filter = new PointFilter(false);
		assertFalse(filter.filter(TRUTHY_POINT));
	}
	
	@Test
	public void testRejectsFalsyPoint() throws Exception {
		PointFilter filter = new PointFilter(true);
		assertFalse(filter.filter(FALSY_POINT));
	}
	
}
