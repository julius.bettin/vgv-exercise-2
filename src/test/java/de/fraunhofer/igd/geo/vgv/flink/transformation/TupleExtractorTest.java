package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Point;
import de.fraunhofer.igd.geo.vgv.flink.util.PointFactory;

public class TupleExtractorTest {

	@Test
	public void testExtractsPointCorrectly() throws Exception {
		TupleExtractor extractor = new TupleExtractor();
		Point point = PointFactory.createPoint(0.0, 1.0, 2.0);
		Tuple2<Boolean, Point> value = new Tuple2<>(true, point);
		Point result = extractor.map(value);
		assertTrue(result.equals(point));
	}
	
}
