package de.fraunhofer.igd.geo.vgv.flink;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class GridTest {

	@Test
	public void testCellDimensionsInitializeCorrectly() {
		Rectangle boundingBox = new Rectangle(0.0, 0.0, 100.0, 50.0);
		Grid grid = new Grid(boundingBox, 10, 10);
		assertEquals(10.0, grid.cellWidth, 0.000001);
		assertEquals(5.0, grid.cellHeight, 0.000001);
	}
	
	@Test
	public void testCellCountsInitializeCorrectly() {
		Rectangle boundingBox = new Rectangle(0.0, 0.0, 100.0, 100.0);
		Grid grid = new Grid(boundingBox, 10, 5);
		assertEquals(10, grid.cellCountWidth);
		assertEquals(5, grid.cellCountHeight);
	}
	
}
